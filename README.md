#DownloadModule
Demo https://git.oschina.net/bvin/AndroidTest_Download.git

权限
--------
```
    //网络
    <uses-permission android:name="android.permission.INTERNET" />
    //下载管理器的访问权限
    <uses-permission android:name="cn.bvin.permission.ACCESS_DOWNLOAD_MANAGER" />
    //访问网络状态
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    //唤醒
    <uses-permission android:name="android.permission.WAKE_LOCK" />
    //外部储存    
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
```
组件
--------
```xml
    //DownloadProvider 必须
    <provider 
        android:name="cn.bvin.lib.module.downloads.DownloadProvider"      android:authorities="cn.bvin.lib.module.downloads" />
    //DownloadService 必须   
    <service 
        android:name="cn.bvin.lib.module.downloads.DownloadService" />
    //DownloadReceiver 非必须
    <receiver
        android:name="cn.bvin.lib.module.downloads.DownloadReceiver"
        android:exported="false" >
            <intent-filter>
                <action android:name="android.intent.action.BOOT_COMPLETED" />
                <action android:name="android.net.conn.CONNECTIVITY_CHANGE" />
            </intent-filter>
    </receiver>
```
使用
--------

```Java
    //开始下载
    DownloadManager dm = new DownloadManager(getContentResolver(), getPackageName());
    DownloadManager.Request request = new Request(srcUri);
			request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "/");
    long currentDownloadId = dm.enqueue(request);

    //暂停
    dm.pauseDownload(currentDownloadId);

    //恢复
    dm.resumeDownload(currentDownloadId);

    //取消或删除
    dm.remove(currentDownloadId);

    //查询，通过cursor可以查询所有下载字段信息
    DownloadManager.Query query = new Query();
    query.setFilterById(currentDownloadId);
    Cursor cursor = dm.query(query);
    
    //也可以通过下面方式来查询
    Uri uri = ContentUris.withAppendedId(Downloads.CONTENT_URI, currentDownloadId);
    Cursor cursor = getContentResolver().query(uri, new String[]{Downloads.COLUMN_CURRENT_BYTES,
				Downloads.COLUMN_TOTAL_BYTES}, null, null, null);
    
    //通过ContentObserver来监听当前下载任务
    getContentResolver().registerContentObserver(Downloads.CONTENT_URI, true, observer);
    
```
下载数据库表字段说明
--------
通过Cursor可以把以下信息查出来

| uri| _data  | status |total_bytes|current_bytes|title
|------------|-----------|--------|--------|--------|
| 下载地址   | 下载文件路径| 下载状态 |总共大小|当前下载字节|下载文件标题
[1]: https://git.oschina.net/bvin/AndroidTest_Download.git
[2]: https://github.com/bumptech/glide/wiki
[3]: http://bumptech.github.io/glide/javadocs/latest/index.html
[4]: https://www.jetbrains.com/idea/download/